<h1>Party Parrot App</h1>

<img src='media/images/party-parrot.gif' alt='parrot' height="200" width="200">
<br>
<br>
<h3></h3>

Sample Python application on Django with PostgreSQL database.

<h3>Install Docker</h3>

____

* install docker https://docs.docker.com/engine/install/
<h3>Deployment</h3>

____

<h3>Deployment application</h3>

* clone repo

* change directory

```
      cd party_parrots
```

* up db container:

```
      docker compose up -d db
```

* create pg_user, pg_database:

```
      docker exec -it party_parrots-db-1 sh
      su postgres
      psql
      CREATE DATABASE app;
      exit
      exit
      exit
```

* up application container:

```
      docker compose up -d
```

* open web-page: http://localhost:8000